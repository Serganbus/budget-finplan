<?php

namespace Serganbus\Budget;

use PHPUnit\Framework\TestCase;

/**
 * Description of CashFlowByDateTest
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class CashFlowByDateTest extends TestCase
{
    /**
     * @var CashFlowInterface
     */
    private $cashflow;
    
    public function setUp(): void
    {
        $this->cashflow = new CashFlowByDate([
            '2019-12-31' => 500,
            '2020-01-01' => 1000,
            '2020-01-05' => 1500,
            '2020-01-17' => 2000,
            '2020-01-31' => 2500,
            '2020-02-01' => 3000,
        ], 'тестовый денежный поток');
    }
    
    public function test__constructInvalidDateFormat()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->cashflow = new CashFlowByDate([
            '31' => -500,
        ], 'тестовый денежный поток');
    }
    
    public function test__constructInvalidAmount()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->cashflow = new CashFlowByDate([
            '2020-12-31' => "-500",
        ], 'тестовый денежный поток');
    }
    
    public function getAmountBetweenDatesDataProvider()
    {
        return [
            [new \DateTime('2020-01-01'), new \DateTime('2020-01-31'), 7000],
            [new \DateTime('2020-01-01'), new \DateTime('2020-12-31'), 10000],
            [new \DateTime('2019-01-01'), new \DateTime('2020-12-31'), 10500],
        ];
    }
    
    /**
     * @dataProvider getAmountBetweenDatesDataProvider
     */
    public function testGetAmountBetweenDates($from, $to, $expectedAmount)
    {
        $this->assertEquals($expectedAmount, $this->cashflow->getAmountBetweenDates($from, $to));
    }
    
    public function getAmountByDateDataProvider()
    {
        return [
            [new \DateTime('2020-01-01'), 1000],
            [new \DateTime('2020-01-31'), 2500],
            [new \DateTime('2022-01-31'), 0],
        ];
    }
    
    /**
     * @dataProvider getAmountByDateDataProvider
     */
    public function testGetAmountByDate($date, $expectedAmount)
    {
        $this->assertEquals($expectedAmount, $this->cashflow->getAmountByDate($date));
    }
    
    public function testGetDescription()
    {
        $this->assertEquals('тестовый денежный поток', $this->cashflow->getDescription());
    }
    
    public function tearDown(): void
    {
        $this->cashflow = null;
    }
}