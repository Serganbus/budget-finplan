<?php

namespace Serganbus\Budget;

use PHPUnit\Framework\TestCase;
use DateTime;

/**
 * Description of FixedMonthlyCashFlowTest
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class FixedMonthlyCashFlowTest extends TestCase
{
    /**
     * @var CashFlowInterface
     */
    private $cashflow;
    
    public function setUp(): void
    {
        $from = new DateTime('2020-01-01 00:00:00');
        $to = new DateTime('2020-03-01 23:59:59');
        
        $this->cashflow = new FixedMonthlyCashFlow([
            1 => 500,
            15 => 1000,
            28 => 1500,
            29 => 2000,
            30 => 2500,
            31 => 3000,
        ], $from, $to, 'тестовый денежный поток');
    }
    
    public function constructInvalidArgumentExDataProvider()
    {
        $from = new DateTime('2020-01-01');
        $to = new DateTime('2020-02-01');
        return [
            [[0 => 100500], $from, $to], // номер дня меньше минимального значения
            [[32 => 100500], $from, $to], // номер дня больше минимального значения
            [[5 => '100500'], $from, $to], // сумма - не целочисленное число
            [[5 => 100.500], $from, $to], // сумма - не целочисленное число
        ];
    }
    
    /**
     * @dataProvider constructInvalidArgumentExDataProvider
     */
    public function test__constructInvalidArgEx($cashflowArr, $from, $to)
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->cashflow = new FixedMonthlyCashFlow($cashflowArr, $from, $to);
    }
    
    public function getAmountBetweenDatesDataProvider()
    {
        return [
            [new \DateTime('2020-01-01'), new \DateTime('2020-01-31'), 10500],
            [new \DateTime('2020-02-01'), new \DateTime('2020-02-28'), 3000],
            [new \DateTime('2020-02-01'), new \DateTime('2020-02-29'), 10500],
            [new \DateTime('2020-01-01'), new \DateTime('2020-12-31'), 21500],
            [new \DateTime('2019-01-01'), new \DateTime('2020-12-31'), 21500],
        ];
    }
    
    /**
     * @dataProvider getAmountBetweenDatesDataProvider
     */
    public function testGetAmountBetweenDates($from, $to, $expectedAmount)
    {
        $this->assertEquals($expectedAmount, $this->cashflow->getAmountBetweenDates($from, $to));
    }
    
    public function getAmountByDateDataProvider()
    {
        return [
            [new \DateTime('2020-01-01'), 500],
            [new \DateTime('2020-02-28'), 1500],
            [new \DateTime('2020-02-29'), 7500],
            [new \DateTime('2020-02-30'), 500], //1е марта
            [new \DateTime('2022-01-31'), 0],
        ];
    }
    
    /**
     * @dataProvider getAmountByDateDataProvider
     */
    public function testGetAmountByDate($date, $expectedAmount)
    {
        $this->assertEquals($expectedAmount, $this->cashflow->getAmountByDate($date));
    }
    
    public function testGetDescription()
    {
        $this->assertEquals('тестовый денежный поток', $this->cashflow->getDescription());
    }
    
    public function tearDown(): void
    {
        $this->cashflow = null;
    }
}