<?php

namespace Serganbus\Budget;

use Serganbus\Money\Credits\CreditParams;
use PHPUnit\Framework\TestCase;
use DateTime;

/**
 * Тесты на таймлайн
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class TimelineTest extends TestCase
{
    /**
     *
     * @var Timeline
     */
    private $timeline;
    
    public function setUp(): void
    {
        $from = new DateTime('2021-01-01');
        $to = new DateTime('2021-12-31');
        $initialMoney = 15000000;
        $this->timeline = new Timeline($from, $to, $initialMoney);
    }
    
    public function testAddCashflow()
    {
        $cashflow = new CashFlowByDate([
            '2021-06-01' => 2000000,
        ], 'Кешбек за НДС');
        $result = $this->timeline->addCashflow($cashflow);
        $this->assertInstanceOf(Timeline::class, $result);
    }
    
    public function testAddCredit()
    {
        $creditFrom = new DateTime('2018-12-03 00:00:00');
        $creditParams = new CreditParams($creditFrom, 263460000, 930, 242, CreditParams::DURATION_MONTH);
        $result = $this->timeline->addCredit($creditParams);
        $this->assertInstanceOf(Timeline::class, $result);
    }
    
    public function getDataProvider()
    {
        $initialMoney = 40000000;
        $fromDate = new DateTime('2021-01-01');
        $toDate = new DateTime('2021-12-31');
        $timeline = new Timeline($fromDate, $toDate, $initialMoney);
        $ipoteka = new CreditParams(new DateTime('2018-12-03 00:00:00'), 263460000, 930, 242, CreditParams::DURATION_MONTH);
        $timeline->addCredit($ipoteka);
        
        $timeline->addCashflow(new FixedMonthlyCashFlow([
            '1' => 15000000
        ]), $fromDate, $toDate, 'Зарплата');
        $timeline->addCashflow(new FixedMonthlyCashFlow([
            '1' => -9400000
        ]), $fromDate, $toDate, 'Ежемесячные расходы');
        $timeline->addCashflow(new CashFlowByDate([
            '2021-06-15' => 1500000,
        ], 'Премия'));
        $timeline->addCashflow(new CashFlowByDate([
            '2021-06-25' => -1300000,
            '2021-07-01' => -700000,
            '2021-07-15' => -1500000,
            '2021-08-01' => -10000000,
            '2021-08-14' => -1500000,
            '2021-08-15' => -3000000,
            '2021-09-15' => -10000000,
            '2021-10-01' => -4000000,
            '2021-12-15' => -1500000,
        ]));
        return [
            [$timeline, Timeline::TIMELINE_MONTHLY, [
                '2021-01-01' => 40000000,
                '2021-02-01' => 43185392,
                '2021-03-01' => 46370784,
                '2021-04-01' => 49556176,
                '2021-05-01' => 52741568,
                '2021-06-01' => 55926960,
                '2021-07-01' => 58612352,
                '2021-08-01' => 50297744,
                '2021-09-01' => 48983136,
                '2021-10-01' => 38168528,
                '2021-11-01' => 41353920,
                '2021-12-01' => 44539312,
                '2021-12-31' => 40624704,
            ]],
        ];
    }
    
    /**
     * @dataProvider getDataProvider
     */
    public function testGet($timeline, $timelineType, $expectedLog)
    {
        $arraysAreSimilar = function ($a, $b) {
            // if the indexes don't match, return immediately
            if (count(array_diff_assoc($a, $b))) {
                return false;
            }
            // we know that the indexes, but maybe not values, match.
            // compare the values between the two arrays
            foreach ($a as $k => $v) {
                if ($v !== $b[$k]) {
                    return false;
                }
            }
            // we have identical indexes, and no unequal values
            return true;
        };
        
        $actualLog = $timeline->get($timelineType);
        
        $this->assertTrue($arraysAreSimilar($expectedLog, $actualLog));
    }
    
    public function tearDown(): void
    {
        $this->timeline = null;
    }
}
