<?php

namespace Serganbus\Budget;

use DateTime;
use DateInterval;
use Serganbus\Money\Credits\CreditParams;
use Serganbus\Money\Credits\RepaymentSchedule;
use Serganbus\Money\Credits\Calculator as CreditCalculator;

/**
 * Построение таймлайна по бюджету
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class Timeline
{
    public const TIMELINE_DAILY = 'DAILY';
    public const TIMELINE_MONTHLY = 'MONTHLY';
    
    /** @var array */
    private $cashflows = [];
    
    /** @var array */
    private $credits = [];
    
    /** @var DateTime */
    private $from;
    
    /** @var DateTime */
    private $to;
    
    /** @var int */
    private $initialMoney;
    
    /** @var CreditCalculator */
    private $creditCalculator;
    
    public function __construct(DateTime $from, DateTime $to, int $initialMoney)
    {
        $this->from = $from;
        $this->to = $to;
        $this->initialMoney = $initialMoney;
        
        $this->creditCalculator = new CreditCalculator();
    }
    
    /**
     * Добавить денежный поток для учета в расчетах
     * 
     * @param \Serganbus\Budget\CashFlowInterface $cashflow
     */
    public function addCashflow(CashFlowInterface $cashflow)
    {
        $this->cashflows[] = $cashflow;
        
        return $this;
    }
    
    /**
     * Добавить кредит для учета в расчетах
     * @param CreditParams $credit
     * @param array $payments
     * @param int $type
     */
    public function addCredit(CreditParams $credit, array $payments = [], int $type = CreditCalculator::TYPE_ANNUITY)
    {
        $this->credits[] = $credit;
        
        $fromTs = $this->from->format('U');
        $toTs = $this->to->format('U');
        
        $repaymentsByDate = [];
        /** @var RepaymentSchedule $repaymentSchedule */
        $repaymentSchedule = $this->creditCalculator->calculate($credit, $payments, $type);
        foreach ($repaymentSchedule as $repayment) {
            /** @var DateTime $date */
            $date = $repayment->getDate();
            if ($date->format('U') >= $fromTs && $date->format('U') <= $toTs) {
                $payment = $repayment->getPayment();
                $repaymentsByDate[$date->format('Y-m-d')] = -$payment;
            }
        }
        
        $this->addCashflow(new CashFlowByDate($repaymentsByDate));
        
        return $this;
    }
    
    /**
     * Получить изменения в располагаемых средствах в течение времени
     * @param string $timelineType
     * @return array
     */
    public function get(string $timelineType = self::TIMELINE_DAILY): array
    {
        if ($timelineType !== self::TIMELINE_DAILY 
            && $timelineType !== self::TIMELINE_MONTHLY) {
            $timelineType = self::TIMELINE_DAILY;
        }
        
        $log = [];
        $money = $this->initialMoney;
        $diffInterval = $this->to->diff($this->from);
        $iterateInterval = new DateInterval('P1D');
        $currentDate = clone $this->from;
        $log[$currentDate->format('Y-m-d')] = $money;
        for ($i = 0; $i < $diffInterval->days; $i++) {
            $currentDate->add($iterateInterval);
            
            $cashflowInDay = 0;
            foreach ($this->cashflows as $cashflow) {
                $cashflowAmount = $cashflow->getAmountByDate($currentDate);
                $cashflowInDay += $cashflowAmount;
            }
            $money += $cashflowInDay;
            
            if ($timelineType === self::TIMELINE_DAILY) {
                $log[$currentDate->format('Y-m-d')] = $money;
            }
            if (
                $timelineType === self::TIMELINE_MONTHLY
                && (
                    $currentDate->format('j') === '1' 
                    || $currentDate->format('Y-m-d') === $this->to->format('Y-m-d')
                )) {
                $log[$currentDate->format('Y-m-d')] = $money;
            }
        }
        
        return $log;
    }
}
