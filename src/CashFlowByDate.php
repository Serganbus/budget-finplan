<?php

namespace Serganbus\Budget;

/**
 * Денежный поток в конкретный день.
 * Например, получение премии или получение разового пособия.
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class CashFlowByDate implements CashFlowInterface
{
    /**
     * @var array
     */
    private $cashflow = [];
    
    /**
     * @var string
     */
    private $description;
    
    /**
     * @param array $cashflow Массив разовых денежных потоков.
     * @param string $description Описание денежного потока
     */
    public function __construct(array $cashflow, string $description = '')
    {
        foreach ($cashflow as $dateStr => $amount) {
            $date = \DateTime::createFromFormat('Y-m-d', $dateStr);
            if (!$date) {
                throw new \InvalidArgumentException("Некорректный формат даты");
            }
            if (!is_int($amount)) {
                throw new \InvalidArgumentException("Некорректная сумма");
            }
        }
        $this->cashflow = $cashflow;
        $this->description = $description;
    }
    
    /**
     * @inheritdoc
     */
    public function getAmountBetweenDates(\DateTime $from, \DateTime $to): int
    {
        $cumulativeAmount = 0;
        
        $diffInterval = $to->diff($from);
        $iterateInterval = new \DateInterval('P1D');
        $currentDate = clone $from;
        for ($i = 0; $i <= $diffInterval->days; $i++) {
            if (isset($this->cashflow[$currentDate->format('Y-m-d')])) {
                $cumulativeAmount += $this->cashflow[$currentDate->format('Y-m-d')];
            }
            
            $currentDate->add($iterateInterval);
        }
        
        return $cumulativeAmount;
    }

    /**
     * @inheritdoc
     */
    public function getAmountByDate(\DateTime $date): int
    {
        $dayOfMonth = $date->format('Y-m-d');
        foreach ($this->cashflow as $dayNum => $amount) {
            if ($dayOfMonth === $dayNum) {
                return $amount;
            }
        }
        
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
