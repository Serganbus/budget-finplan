<?php

namespace Serganbus\Budget;

/**
 * Интерфейс описывающий движение денег на дату
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
interface CashFlowInterface
{
    /**
     * Получить сумму доходов/расходов за день. В копейках
     * 
     * @param \DateTime $date
     * @return int
     */
    public function getAmountByDate(\DateTime $date): int;
    
    /**
     * Поучить сумму доходов/расходов за временной промежуток. В копейках
     * 
     * @param \DateTime $from
     * @param \DateTime $to
     * @return int
     */
    public function getAmountBetweenDates(\DateTime $from, \DateTime $to): int;
    
    /**
     * Получить описание денежного потока
     * 
     * @return string
     */
    public function getDescription(): string;
}
