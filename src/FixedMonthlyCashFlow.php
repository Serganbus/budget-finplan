<?php

namespace Serganbus\Budget;

use DateTime;

/**
 * Фиксированный ежемесячный денежный поток. 
 * Например, зарплата или возврат долга. 
 * Или, наоборот, стабильные ежемесячные траты.
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class FixedMonthlyCashFlow implements CashFlowInterface
{
    private $cashflow = [];
    
    /** @var DateTime|null */
    private $from;
    
    /** @var DateTime|null */
    private $to;
    
    /** @var string */
    private $description;
    
    /**
     * 
     * @param array $cashflow Массив стабильных денежных потоков.
     * Ключ - порядковый номер дня в месяце, а значение - сумма.
     * 
     */
    public function __construct(array $cashflow, DateTime $from = null, DateTime $to = null, string $description = '')
    {
        foreach ($cashflow as $numOfDay => $amount) {
            if ($numOfDay < 1 || $numOfDay > 31) {
                throw new \InvalidArgumentException("Некорректный порядковый номер дня");
            }
            if (!is_int($amount)) {
                throw new \InvalidArgumentException("Некорректная сумма");
            }
        }
        $this->cashflow = $cashflow;
        $this->from = $from;
        $this->to = $to;
        $this->description = $description;
    }
    
    /**
     * @inheritdoc
     */
    public function getAmountBetweenDates(DateTime $from, DateTime $to): int
    {
        if (isset($this->from) && $from < $this->from) {
            $from = $this->from;
        }
        
        if (isset($this->to) && $to > $this->to) {
            $to = $this->to;
        }
        
        $cumulativeAmount = 0;
        
        $diffInterval = $to->diff($from);
        $iterateInterval = new \DateInterval('P1D');
        $currentDate = clone $from;
        for ($i = 0; $i <= $diffInterval->days; $i++) {
            $cumulativeAmount += $this->getAmountByDate($currentDate);
            
            $currentDate->add($iterateInterval);
        }
        
        return $cumulativeAmount;
    }

    /**
     * @inheritdoc
     */
    public function getAmountByDate(DateTime $date): int
    {
        if (isset($this->from) 
            && isset($this->to) 
            && $date > $this->from 
            && $date > $this->to) {
            return 0;
        }
        
        $amount = 0;
        
        $dayOfMonth = $date->format('j');
        $daysInMonth = $date->format('t');
        foreach ($this->cashflow as $dayNum => $cashflowAmount) {
            if ($dayOfMonth === (string)$dayNum) {
                $amount += $cashflowAmount;
            }
            // если, например, задана дата 28е февраля, 
            // а есть постоянные выплаты на 29, 30 и 31 числа, 
            // то эти выплаты попадают в 28е февраля
            if ($daysInMonth < $dayNum && $dayOfMonth === $daysInMonth) {
                $amount += $cashflowAmount;
            }
        }
        
        return $amount;
    }
    
    /**
     * @inheritdoc
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
